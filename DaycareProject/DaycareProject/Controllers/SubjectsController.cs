﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace DaycareProject.Models
{
    public class SubjectsController : Controller
    {
        private readonly daycarelggoContext _context;

        public SubjectsController(daycarelggoContext context)
        {
            _context = context;
        }

        // GET: Subjects
        public async Task<IActionResult> Index()
        {
            var daycarelggoContext = _context.Subjects.Include(s => s.ParentNavigation);
            return View(await daycarelggoContext.ToListAsync());
        }

        // GET: Subjects/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subjects = await _context.Subjects
                .Include(s => s.ParentNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (subjects == null)
            {
                return NotFound();
            }

            return View(subjects);
        }

        // GET: Subjects/Create
        public IActionResult Create()
        {
            ViewData["Parent"] = new SelectList(_context.Parents, "Id", "Email");
            return View();
        }

        // POST: Subjects/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title")] Subjects subjects)
        {
            if (ModelState.IsValid)
            {
                subjects.CreatedTs = DateTime.Now;
                if (HttpContext.Session.GetInt32("SessionUserId") == null) //TODO Create an autorization checking
                {
                    return NotFound();
                }
                subjects.Parent = (int)HttpContext.Session.GetInt32("SessionUserId");
                _context.Add(subjects);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Parent"] = new SelectList(_context.Parents, "Id", "Email", subjects.Parent);
            return View(subjects);
        }

        // GET: Subjects/ViewAddComments/5
        public async Task<IActionResult> ViewAddComments(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewData["subjectId"] = id;
            List<Comments> commentList = await _context.Comments.Where(c => c.Subject == id).Include(c => c.SubjectNavigation).Include(c => c.ParentNavigation).ToListAsync();
            var subject = await _context.Subjects.Include(c => c.ParentNavigation).FirstOrDefaultAsync(m => m.Id == id);
            ViewBag.Subject = subject;

            return View(commentList);
        }


        // GET: Subjects/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subjects = await _context.Subjects.FindAsync(id);
            if (subjects == null)
            {
                return NotFound();
            }
            ViewData["Parent"] = new SelectList(_context.Parents, "Id", "Email", subjects.Parent);
            return View(subjects);
        }

        // POST: Subjects/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Parent,Title,CreatedTs")] Subjects subjects)
        {
            if (id != subjects.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(subjects);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubjectsExists(subjects.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Parent"] = new SelectList(_context.Parents, "Id", "Email", subjects.Parent);
            return View(subjects);
        }

        // GET: Subjects/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subjects = await _context.Subjects
                .Include(s => s.ParentNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (subjects == null)
            {
                return NotFound();
            }

            return View(subjects);
        }

        // POST: Subjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var subjects = await _context.Subjects.FindAsync(id);
            _context.Subjects.Remove(subjects);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SubjectsExists(int id)
        {
            return _context.Subjects.Any(e => e.Id == id);
        }
    }
}
