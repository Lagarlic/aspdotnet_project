﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace DaycareProject.Models
{
    public class ChildrenController : Controller
    {
        private readonly daycarelggoContext _context;

        public ChildrenController(daycarelggoContext context)
        {
            _context = context;
        }

        // GET: Children
        public async Task<IActionResult> Index()
        {
            //var daycarelggoContext = _context.Children.Include(c => c.CurrentGroupNavigation).Include(c => c.ParentNavigation);
            //return View(await daycarelggoContext.ToListAsync());

            List<Children> childrenList = await _context.Children.Where(c => c.Parent == HttpContext.Session.GetInt32("SessionUserId")).Include(c => c.CurrentGroupNavigation).Include(c => c.ParentNavigation).ToListAsync();
            foreach (Children c in childrenList)
            {
                if (c.Photo != null)
                {
                    string imageBase64Data = Convert.ToBase64String(c.Photo);
                    string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                      
                    ViewData["imgChild" + c.Id] = imageDataURL;
                }
            }
            return View(childrenList);
        }


        public async Task<IActionResult> ReportCurrentWorkload()
        {
            List<Children> childrenList = await _context.Children.Where(c => c.Status == ChildrenStatusEnum.Attend).Include(c => c.CurrentGroupNavigation).Include(c => c.ParentNavigation).ToListAsync();
            foreach (Children c in childrenList)
            {
                if (c.Photo != null)
                {
                    string imageBase64Data = Convert.ToBase64String(c.Photo);
                    string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);

                    ViewData["imgChildReport" + c.Id] = imageDataURL;
                }
            }
            return View(childrenList);
        }
        public async Task<IActionResult> ReportWaitingList()
        {
            List<Children> childrenList = await _context.Children.Where(c => c.Status == ChildrenStatusEnum.Pending).Include(c => c.CurrentGroupNavigation).Include(c => c.ParentNavigation).ToListAsync();
            foreach (Children c in childrenList)
            {
                if (c.Photo != null)
                {
                    string imageBase64Data = Convert.ToBase64String(c.Photo);
                    string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);

                    ViewData["imgChildReport" + c.Id] = imageDataURL;
                }
            }
            return View(childrenList);
        }



        // GET: Children/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var children = await _context.Children
                .Include(c => c.CurrentGroupNavigation)
                .Include(c => c.ParentNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (children == null)
            {
                return NotFound();
            }
            if (children.Photo != null)
            {
                string imageBase64Data = Convert.ToBase64String(children.Photo);
            string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
            ViewData["imgChildDetail"] = imageDataURL;
                }
            return View(children);
        }

        // GET: Children/Create
        public IActionResult Create()
        {
            ViewData["CurrentGroup"] = new SelectList(_context.Groups, "Id", "Description");
            ViewData["Parent"] = new SelectList(_context.Parents, "Id", "Email");
            return View();
        }

        // POST: Children/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Parent,FirstName,LastName,Gender,Birthdate,Photo,CurrentGroup,Status,Notes,RegisterTs")] Children children)
        {
            foreach (var file in Request.Form.Files)
            {
                MemoryStream ms = new MemoryStream();
                file.CopyTo(ms);
                children.Photo = ms.ToArray();
                ms.Close();
                ms.Dispose();
            }
            if (HttpContext.Session.GetInt32("SessionUserId") == null) //TODO Create an autorization checking
            {
                return NotFound();
            }
            children.Parent = (int) HttpContext.Session.GetInt32("SessionUserId");            
            children.RegisterTs = DateTime.Now;
            children.Status = ChildrenStatusEnum.Pending;
            if (ModelState.IsValid)
            {
                _context.Add(children);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CurrentGroup"] = new SelectList(_context.Groups, "Id", "Description", children.CurrentGroup);
            ViewData["Parent"] = new SelectList(_context.Parents, "Id", "Email", children.Parent);
            return View(children);
        }

        // GET: Children/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var children = await _context.Children.FindAsync(id);
            if (children == null)
            {
                return NotFound();
            }
            if (children.Photo != null)
            {
                string imageBase64Data = Convert.ToBase64String(children.Photo);
                string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                ViewData["imgChildEdit"] = imageDataURL;
            }
            ViewData["CurrentGroup"] = new SelectList(_context.Groups, "Id", "Description", children.CurrentGroup);
            ViewData["Parent"] = new SelectList(_context.Parents, "Id", "Email", children.Parent);
            return View(children);
        }

        // POST: Children/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Parent,FirstName,LastName,Gender,Birthdate,Photo,CurrentGroup,Status,Notes,RegisterTs")] Children children)
        {
            if (id != children.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                //photo
                foreach (var file in Request.Form.Files)
                {
                    MemoryStream ms = new MemoryStream();
                    file.CopyTo(ms);
                    children.Photo = ms.ToArray();
                    ms.Close();
                    ms.Dispose();
                }

                try
                {
                    _context.Update(children);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ChildrenExists(children.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CurrentGroup"] = new SelectList(_context.Groups, "Id", "Description", children.CurrentGroup);
            ViewData["Parent"] = new SelectList(_context.Parents, "Id", "Email", children.Parent);
            return View(children);
        }

        // GET: Children/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var children = await _context.Children
                .Include(c => c.CurrentGroupNavigation)
                .Include(c => c.ParentNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (children == null)
            {
                return NotFound();
            }
            if (children.Photo != null)
            {
                string imageBase64Data = Convert.ToBase64String(children.Photo);
                string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                ViewData["imgChildDetail"] = imageDataURL;
            }
            return View(children);
        }

        // POST: Children/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var children = await _context.Children.FindAsync(id);
            _context.Children.Remove(children);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ChildrenExists(int id)
        {
            return _context.Children.Any(e => e.Id == id);
        }
    }
}
