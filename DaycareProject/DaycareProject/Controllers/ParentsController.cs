﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Web;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace DaycareProject.Models
{    
    public class ParentsController : Controller
    {
        private readonly daycarelggoContext _context;
        /*
        public ParentsController(daycarelggoContext context)
        {
            _context = context;
        }
        */
        IConfiguration _configuration;

        public ParentsController(IConfiguration configuration, daycarelggoContext context)
        {
            _configuration = configuration;
            _context = context;
        }

        // GET: Parents
        public async Task<IActionResult> Index()
        {
            List < Parents > parentList = await _context.Parents.ToListAsync();
            foreach(Parents p in parentList)
            {
                if (p.Photo != null)
                {
                    string imageBase64Data = Convert.ToBase64String(p.Photo);
                    string imageDataURL = string.Format("data:image/jpg;base64,{0}",imageBase64Data);                    
                    ViewData["img" + p.Id] = imageDataURL;
                }
            }
            return View(parentList);            
        }

        // GET: Parents/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parents = await _context.Parents
                .FirstOrDefaultAsync(m => m.Id == id);
            if (parents == null)
            {
                return NotFound();
            }

            return View(parents);
        }

        // GET: Parents/Create
        public IActionResult Create()
        {
            //reCaptcha            
            ViewData["ReCaptchaKey"] = _configuration.GetSection("GoogleReCaptcha:key").Value;

            return View();
        }

        // POST: Parents/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirsName,LastName,Email,Password,PasswordRepeat,PhoneNo,Street,City,Province,Country,PostalCode,Verified,Token,Gender,Photo")] Parents parents)
        {

            //reCaptcha            
            ViewData["ReCaptchaKey"] = _configuration.GetSection("GoogleReCaptcha:key").Value;

            if (ModelState.IsValid)
            {
                //store the image in byte[] format in DB
                foreach (var file in Request.Form.Files)
                {
                    MemoryStream ms = new MemoryStream();
                    file.CopyTo(ms);
                    parents.Photo = ms.ToArray();
                    ms.Close();
                    ms.Dispose();
                }
                var emailVerification = await _context.Parents.FirstOrDefaultAsync(x => x.Email == parents.Email);
                if (emailVerification != null)
                {
                    ModelState.AddModelError(string.Empty, "The email is registered, try enother one!");
                    return View(parents);
                }

                //recaptcha
                if (!ReCaptchaPassed(
            Request.Form["g-recaptcha-response"], // that's how you get it from the Request object
            _configuration.GetSection("GoogleReCaptcha:secret").Value
            ))
                {
                    ModelState.AddModelError(string.Empty, "You failed the CAPTCHA!!!");
                    return View(parents);
                }
                //
                _context.Add(parents);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Login));
            }
            return View(parents);
        }

        // GET: Parents/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parents = await _context.Parents.FindAsync(id);
            if (parents == null)
            {
                return NotFound();
            }
            return View(parents);
        }

        // POST: Parents/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirsName,LastName,Email,Password,PhoneNo,Street,City,Province,Country,PostalCode,Verified,Token,Gender,Photo")] Parents parents)
        {
            if (id != parents.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(parents);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ParentsExists(parents.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(parents);
        }

        // GET: Parents/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parents = await _context.Parents
                .FirstOrDefaultAsync(m => m.Id == id);
            if (parents == null)
            {
                return NotFound();
            }

            return View(parents);
        }

        // POST: Parents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var parents = await _context.Parents.FindAsync(id);
            _context.Parents.Remove(parents);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ParentsExists(int id)
        {
            return _context.Parents.Any(e => e.Id == id);
        }

        // GET: Parents/Login
        public IActionResult Login()
        {
            return View();
        }

        
        [HttpPost]
        public async Task<IActionResult> Login([Bind("Email,Password")] Parents parents)
        {   
                var parent = await _context.Parents.FirstOrDefaultAsync(x => x.Email == parents.Email && x.Password == parents.Password);

                if (parent == null)
                {                    
                    ModelState.AddModelError(string.Empty, "Invalid username or password");                    
                }
                else
                {
                if (parent.Photo != null)
                {
                    string imageBase64Data = Convert.ToBase64String(parent.Photo);
                    string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                    //ViewBag.ImageDataUrl = imageDataURL;  SessionUserPhoto
                    HttpContext.Session.SetString("SessionUserPhoto", imageDataURL); 
                }
                HttpContext.Session.SetString("SessionUserName", parent.FirsName + " " + parent.LastName);
                HttpContext.Session.SetInt32("SessionUserId", parent.Id);                
                return RedirectToAction("Index", "Home");                   
                }
            
            return View(parents);

        }
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("SessionUserName");
            HttpContext.Session.Remove("SessionUserId");
            HttpContext.Session.Remove("SessionUserPhoto");
            return RedirectToAction("Index", "Home");
        }

        //Recaptcha 
        public static bool ReCaptchaPassed(string gRecaptchaResponse, string secret)
        {
            HttpClient httpClient = new HttpClient();
            var res = httpClient.GetAsync($"https://www.google.com/recaptcha/api/siteverify?secret={secret}&response={gRecaptchaResponse}").Result;
            if (res.StatusCode != HttpStatusCode.OK)
            {
                //logger.LogError("Error while sending request to ReCaptcha");
                return false;
            }

            string JSONres = res.Content.ReadAsStringAsync().Result;
            dynamic JSONdata = JObject.Parse(JSONres);
            if (JSONdata.success != "true")
            {
                return false;
            }

            return true;
        }
        

    }
}
