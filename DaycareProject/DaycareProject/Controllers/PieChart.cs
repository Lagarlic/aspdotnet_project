﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DaycareProject.Models;
using Microsoft.AspNetCore.Hosting;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using System.IO;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Drawing;
using System.Drawing;
// multi lang
using Microsoft.Extensions.Localization;
//------------------------------------------

//using StackedChartwithCoreMVCDemo.Models;

namespace DaycareProject.Controllers
{
    public class PieChartController : Controller
    {

        // GET: /<controller>/  
        public IActionResult Index()
        {
            return View();
        }

        private readonly daycarelggoContext _context;

        public PieChartController(daycarelggoContext context)
        {
            _context = context;
        }

        [HttpGet]
        public JsonResult PopulationChart()
        {

            var populationList = new List<PopulationModel>();

            int pending = _context.Children.Where(c => c.Status == ChildrenStatusEnum.Pending).Count();
            populationList.Add(new PopulationModel { CityName = "Pending", Population = pending });

            int attend = _context.Children.Where(c => c.Status == ChildrenStatusEnum.Attend).Count();
            populationList.Add(new PopulationModel { CityName = "Attend", Population = attend });

            int graduated = _context.Children.Where(c => c.Status == ChildrenStatusEnum.Graduated).Count();
            populationList.Add(new PopulationModel { CityName = "Graduated", Population = graduated });

            return Json(populationList);
        }

        public class PopulationModel
        {
            public string CityName { get; set; }
            public int Population { get; set; }

        }

    }
}
