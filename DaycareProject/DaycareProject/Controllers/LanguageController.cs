﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace DaycareProject.Controllers
{
    public class LanguageController : Controller
    {
       
        // multi lang
        private readonly IStringLocalizer<HomeController> _localizer;

        public LanguageController(IStringLocalizer<HomeController> localizer)
        {
            _localizer = localizer;
        }
        //---------------------
       
        public IActionResult Index()
        {

            return View();
        }
    }
}
