﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DaycareProject.Models;
using Microsoft.AspNetCore.Hosting;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using System.IO;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Drawing;
using System.Drawing;
// multi lang
using Microsoft.Extensions.Localization;
//------------------------------------------

namespace DaycareProject.Controllers
{
    public class PrintingController : Controller
    {

        private readonly IHostingEnvironment _hostingEnvironment;
        public PrintingController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult ExportToPDF()
        {

            //string mvcVersion = typeof(Controller).Assembly.GetName().Version.ToString();

            //Initialize HTML to PDF converter 
            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter();
            WebKitConverterSettings settings = new WebKitConverterSettings();
            //Set WebKit path
            settings.WebKitPath = Path.Combine(_hostingEnvironment.ContentRootPath, "QtBinariesWindows");
            //Assign WebKit settings to HTML converter
            htmlConverter.ConverterSettings = settings;
            //Convert URL to PDF

            //PdfDocument document = htmlConverter.Convert("https://www.google.com");
            PdfDocument document = htmlConverter.Convert("https://localhost:44389/Parents/Index");



            using (MemoryStream stream = new MemoryStream())
            {
                document.Save(stream);
                stream.Position = 0;
                document.Close(true);


                //return File(stream.ToArray(), System.Net.Mime.MediaTypeNames.Application.Pdf, "Output.pdf");

                //Defining the ContentType for pdf file.
                string contentType = "application/pdf";
                //Define the file name.
                string fileName = "Output.pdf";
                //Creates a FileContentResult object by using the file contents, content type, and file name.
                return File(stream.ToArray(), contentType, fileName);


            }

        }

        public IActionResult CreateContacts()
        {

            //Create a new PDF document
            PdfDocument document = new PdfDocument();
            //Add a page to the document
            PdfPage page = document.Pages.Add();
            //Create PDF graphics for the page
            PdfGraphics graphics = page.Graphics;
            //----------------------------------------------------------------



            //Set the standard font
            PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 22);
            //Draw the text           
            graphics.DrawString("Thank you for your interest in our kindergarten!", font, PdfBrushes.Black, new Syncfusion.Drawing.PointF(40, 20));
            PdfFont font2 = new PdfStandardFont(PdfFontFamily.Helvetica, 18);
            graphics.DrawString("Contact us with any questions or clarifications.", font2, PdfBrushes.Black, new Syncfusion.Drawing.PointF(0, 80));
            graphics.DrawString("Civic / Postal Address:", font2, PdfBrushes.Black, new Syncfusion.Drawing.PointF(0, 120));
            PdfFont font3 = new PdfStandardFont(PdfFontFamily.Helvetica, 14);
            graphics.DrawString("21275 Lakeshore Road", font3, PdfBrushes.Black, new Syncfusion.Drawing.PointF(30, 150));
            graphics.DrawString("Sainte-Anne-de-Bellevue, Québec", font3, PdfBrushes.Black, new Syncfusion.Drawing.PointF(30, 170));
            graphics.DrawString("H9X 3L9 Canada", font3, PdfBrushes.Black, new Syncfusion.Drawing.PointF(30, 190));

            graphics.DrawString("Contact Information:", font2, PdfBrushes.Black, new Syncfusion.Drawing.PointF(0, 230));

            graphics.DrawString("Phone Number: 514 - 123 - 1234", font3, PdfBrushes.Black, new Syncfusion.Drawing.PointF(30, 260));
            graphics.DrawString("Fax Number: 514 - 123 - 4567", font3, PdfBrushes.Black, new Syncfusion.Drawing.PointF(30, 280));
            graphics.DrawString("Email: Daycare@daycare.com", font3, PdfBrushes.Black, new Syncfusion.Drawing.PointF(30, 300));


            //Saving the PDF to the MemoryStream
            MemoryStream stream = new MemoryStream();

            document.Save(stream);

            //Set the position as '0'.
            stream.Position = 0;

            //Download the PDF document in the browser
            FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/pdf");

            fileStreamResult.FileDownloadName = "Sample.pdf";

            return fileStreamResult;

        }

        public IActionResult CurrentWorkload()
        {



            //string mvcVersion = typeof(Controller).Assembly.GetName().Version.ToString();

            //Initialize HTML to PDF converter 
            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter();
            WebKitConverterSettings settings = new WebKitConverterSettings();
            //Set WebKit path
            settings.WebKitPath = Path.Combine(_hostingEnvironment.ContentRootPath, "QtBinariesWindows");
            //Assign WebKit settings to HTML converter
            htmlConverter.ConverterSettings = settings;
            //Convert URL to PDF

            //PdfDocument document = htmlConverter.Convert("https://www.google.com");
            PdfDocument document = htmlConverter.Convert("https://localhost:44389/Children/reportCurrentWorkload");



            using (MemoryStream stream = new MemoryStream())
            {
                document.Save(stream);
                stream.Position = 0;
                document.Close(false);


                //return File(stream.ToArray(), System.Net.Mime.MediaTypeNames.Application.Pdf, "Output.pdf");

                //Defining the ContentType for pdf file.
                string contentType = "application/pdf";
                //Define the file name.
                string fileName = "Output.pdf";
                //Creates a FileContentResult object by using the file contents, content type, and file name.
                return File(stream.ToArray(), contentType, fileName);


            }

        }

        public IActionResult WaitingList()
        {

            //string mvcVersion = typeof(Controller).Assembly.GetName().Version.ToString();



            //Initialize HTML to PDF converter 
            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter();
            WebKitConverterSettings settings = new WebKitConverterSettings();
            //Set WebKit path
            settings.WebKitPath = Path.Combine(_hostingEnvironment.ContentRootPath, "QtBinariesWindows");
            //Assign WebKit settings to HTML converter
            htmlConverter.ConverterSettings = settings;
            //Convert URL to PDF       
            PdfDocument document = htmlConverter.Convert("https://localhost:44389/Children/ReportWaitingList");



            using (MemoryStream stream = new MemoryStream())
            {
                document.Save(stream);
                stream.Position = 0;
                document.Close(false);

                //Defining the ContentType for pdf file.
                string contentType = "application/pdf";
                //Define the file name.
                string fileName = "Output.pdf";
                //Creates a FileContentResult object by using the file contents, content type, and file name.
                return File(stream.ToArray(), contentType, fileName);


            }

        }




        public IActionResult Index()
        {
            return View();
        }
    }
}
