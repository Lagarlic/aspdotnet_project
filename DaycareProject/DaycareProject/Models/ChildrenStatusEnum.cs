﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DaycareProject.Models
{
    public enum ChildrenStatusEnum: int
    {
        Pending = 1,
        Attend = 2,
        Graduated = 3
    }
}
