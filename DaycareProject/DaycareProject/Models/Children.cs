﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DaycareProject.Models
{
    public partial class Children
    {
        public int Id { get; set; }
        public int Parent { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z''-' \s]{1,50}$", ErrorMessage = "First name lenght should be 1 - 50 chars long, contain letters, spaces and '-'.")]
        public string FirstName { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z''-' \s]{1,50}$", ErrorMessage = "Last name lenght should be 1 - 50 chars long, contain letters, spaces and '-'.")]
        public string LastName { get; set; }
        //public int? Gender { get; set; }
        public GenderEnum? Gender { get; set; }
        [Required]
        public DateTime Birthdate { get; set; }
        public byte[] Photo { get; set; }
        public int? CurrentGroup { get; set; }
        //public int? Status { get; set; }
        public ChildrenStatusEnum? Status { get; set; }
        public string Notes { get; set; }
        public DateTime RegisterTs { get; set; }

        public virtual Groups CurrentGroupNavigation { get; set; }
        public virtual Parents ParentNavigation { get; set; }
    }
}
