﻿using System;
using System.Collections.Generic;

namespace DaycareProject.Models
{
    public partial class Subjects
    {
        public Subjects()
        {
            Comments = new HashSet<Comments>();
        }

        public int Id { get; set; }
        public int Parent { get; set; }
        public string Title { get; set; }
        public DateTime CreatedTs { get; set; }

        public virtual Parents ParentNavigation { get; set; }
        public virtual ICollection<Comments> Comments { get; set; }
    }
}
