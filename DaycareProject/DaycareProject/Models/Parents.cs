﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaycareProject.Models
{
    public partial class Parents
    {
        public Parents()
        {
            Children = new HashSet<Children>();
            Comments = new HashSet<Comments>();
            Subjects = new HashSet<Subjects>();
        }
       
        public int Id { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z''-' \s]{1,50}$", ErrorMessage = "First name lenght should be 1 - 50 chars long, contain letters, spaces and '-'.")]
        public string FirsName { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z''-' \s]{1,50}$", ErrorMessage = "Last name lenght should be 1 - 50 chars long, contain letters, spaces and '-'.")]
        public string LastName { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [Required]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,15}$", ErrorMessage = "Password should have 6-50 characters, at least one uppercase letter, one lowercase letter and one number")]
        public string Password { get; set; }
        [NotMapped]
        [Compare("Password", ErrorMessage = "Password and Repeat Password do not match")]
        public string PasswordRepeat { get; set; }
        [Required]
        [Phone]
        public string PhoneNo { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public int? Verified { get; set; }
        public string Token { get; set; }
        //public int? Gender { get; set; } 
        public GenderEnum? Gender { get; set; }
        public byte[] Photo { get; set; }        

        public virtual ICollection<Children> Children { get; set; }
        public virtual ICollection<Comments> Comments { get; set; }
        public virtual ICollection<Subjects> Subjects { get; set; }
    }
}
