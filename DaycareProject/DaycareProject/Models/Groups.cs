﻿using System;
using System.Collections.Generic;

namespace DaycareProject.Models
{
    public partial class Groups
    {
        public Groups()
        {
            Children = new HashSet<Children>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Children> Children { get; set; }
    }
}
