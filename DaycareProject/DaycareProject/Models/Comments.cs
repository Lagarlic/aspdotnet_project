﻿using System;
using System.Collections.Generic;

namespace DaycareProject.Models
{
    public partial class Comments
    {
        public int Id { get; set; }
        public int Subject { get; set; }
        public int Parent { get; set; }
        public string Body { get; set; }
        public DateTime CreatedTs { get; set; }

        public virtual Parents ParentNavigation { get; set; }
        public virtual Subjects SubjectNavigation { get; set; }
    }
}
