﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DaycareProject.Models
{
    public enum GenderEnum : int
    {
        Male = 1,
        Female = 2,
        Other = 3
    }
}
